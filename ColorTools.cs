﻿using UnityEngine;
using System.Collections;

namespace ABXY.EditorUIFramework
{
    public class ColorTools
    {
        public static Texture2D MakeTex(Color color)
        {
            Texture2D texture = new Texture2D(1, 1);
            texture.SetPixel(0, 0, color);
            texture.Apply();
            return texture;
        }
    }
}