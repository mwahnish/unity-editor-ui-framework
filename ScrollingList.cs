﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace ABXY.EditorUIFramework
{
    public class ScrollingList
    {

        //public variables
        public List<object[]> listItems = new List<object[]>();

        //This displays a list item
        public delegate float ListElementDisplayDelegate(object[] args, float allowedWidth, float currentHeight, bool display);
        public ListElementDisplayDelegate listElementDelegate;

        //Callback for selection
        public delegate void OnSelectionDelegate(int index, object[] selectedListItem);
        public OnSelectionDelegate OnSelection;

        //Private variables
        private Vector2 scrollPosition = Vector2.zero;
        private Rect cachedDimensions = new Rect(0f, 0f, 1f, 1f);
        private int selectionIndex = 0;

        //configs
        public bool alternateColor = false;
        public bool selectable = false;
        public Color primaryColor = new Color(0.88f, 0.88f, 0.88f);
        public Color secondaryColor = new Color(0.9215f, 0.9215f, 0.9215f);
        public Color selectedColor = new Color(0.6353f, 0.6353f, 0.6353f);

        //testing for updates
        //Cached Variables
        private EditorWindow parentWindowCached;

        //Displays the list. Must be called in OnGUI.
        public void Display(Rect dimensions,EditorWindow parentWindow)
        {
            parentWindowCached = parentWindow;
            cachedDimensions = dimensions;

            // Checking and validating selection actions
            if (selectable)
            {
                ValidateSelectionIndex();
                if (UserDidSelect() && listItems.Count > 0 && OnSelection != null)
                {
                    OnSelection(selectionIndex, listItems[selectionIndex]);
                }
            }


            //Time to draw!
            ColoredSquare.Draw(dimensions, primaryColor);

            scrollPosition = GUI.BeginScrollView(dimensions, scrollPosition, new Rect(0, 0, dimensions.width - 20f, CalcHeight(dimensions.width) ));
            
            float currentHeight = 0f;
            for (int index = 0; index < listItems.Count; index++ )
            {
                float elementHeight = listElementDelegate(listItems[index], dimensions.width - 20f, currentHeight, false);
                if (selectable && index == selectionIndex)
                {
                    ColoredSquare.Draw(new Rect(0f, currentHeight, dimensions.width, elementHeight), selectedColor);
                }else if (alternateColor && index % 2 == 0)
                {
                    ColoredSquare.Draw(new Rect(0f, currentHeight, dimensions.width, elementHeight), secondaryColor);
                }
                float listElementHeight = listElementDelegate(listItems[index], dimensions.width - 20f, currentHeight, true);

                if (selectable)
                    DoMouseSelection(currentHeight, dimensions, listElementHeight, index);

                currentHeight = currentHeight + listElementHeight;
                

            }
            
            GUI.EndScrollView();
            parentWindow.Repaint();
        }

        private void DoMouseSelection(float currentHeight, Rect dimensions, float listElementHeight, int index)
        {
            GUIStyle buttonStyle = EditorStyles.miniButton;
            buttonStyle.normal.background = null;
            buttonStyle.active.background = null;
            if (GUI.Button(new Rect(0, currentHeight, dimensions.width, listElementHeight), "", buttonStyle))
            {
                selectionIndex = index;
                OnSelection(selectionIndex, listItems[selectionIndex]);
            }
            if (Event.current != null && dimensions.Contains(Event.current.mousePosition)){
                Vector2 mousePosition = Event.current.mousePosition;
                Rect transformedRect = new Rect(dimensions.x,dimensions.y + currentHeight - scrollPosition.y, dimensions.width, listElementHeight);
                if (transformedRect.Contains(mousePosition)){
                    selectionIndex = index;
                    
                }
            }
        }


        public void GoToEnd()
        {
            scrollPosition = new Vector2(0, CalcHeight(cachedDimensions.width - 20f));
        }

        public void GoToCurrentIndex()
        {
            if (selectionIndex >= 0 && selectionIndex < listItems.Count)
            {
                float newPosition = CalcHeightAt(selectionIndex, cachedDimensions.width - 20f);
                float selectedElementHeight = listElementDelegate(listItems[selectionIndex], cachedDimensions.width - 20f, 0f, false);
                if (IsSelectionOutOfView(cachedDimensions, newPosition, selectedElementHeight))
                {
                    if (scrollPosition.y <= newPosition)
                    {
                        newPosition = newPosition - cachedDimensions.height + selectedElementHeight;
                        scrollPosition = new Vector2(0, newPosition);
                    }
                    else
                    {
                        scrollPosition = new Vector2(0, newPosition);
                    }
                } 
            }
        
        }

        public void SelectionUp()
        {
            selectionIndex--;
            GoToCurrentIndex();
            parentWindowCached.Repaint();
        }

        public void SelectionDown()
        {
            selectionIndex++;
            GoToCurrentIndex();
            parentWindowCached.Repaint();
            
        }

        public bool UserDidSelect()
        {
            Event currentEvent = Event.current;
            return (currentEvent != null && currentEvent.type == EventType.keyDown && currentEvent.keyCode == KeyCode.Return);
        }

        public object[] GetCurrentSelection()
        {
            object[] result = new object[]{};
            if (listItems.Count > 0){
                ValidateSelectionIndex();
                result = listItems[selectionIndex];
            }
            return result;
        }

        public float CalcContentHeight()
        {
            float height = CalcHeight(cachedDimensions.width);
            return height;
        }

        private float CalcHeight(float allowedWidth)
        {
            float height = 0;
            if (listElementDelegate != null)
            {
                foreach (object[] element in listItems)
                {
                    height = height + listElementDelegate(element, allowedWidth, 0, false);
                }
            }

            return height;
        }

        private float CalcHeightAt(int index, float allowedWidth)
        {
            float height = 0f;
            if (listElementDelegate != null)
            {
                for (int listIndex = 0; listIndex < index && listIndex < listItems.Count; listIndex++ )
                {
                    float listElementHeight = listElementDelegate(listItems[listIndex], allowedWidth, 0, false);
                    height = height + listElementHeight;
                    

                }
            }
            return height;
        }
        
        private bool IsSelectionOutOfView(Rect WindowDimensions, float selectedPosition, float selectionHeight)
        {
            float minBoundary = scrollPosition.y;
            float maxBoundary = scrollPosition.y + WindowDimensions.height;
            float bottomEdge = selectedPosition + selectionHeight;
            return !(minBoundary <= selectedPosition && maxBoundary >= bottomEdge);
        }

        private void ValidateSelectionIndex()
        {
            if (listItems.Count > 0)
            {
                selectionIndex = (int)Mathf.Clamp(selectionIndex, 0f, listItems.Count - 1f);
            }
            else
            {
                selectionIndex = 0;
            }
        }

    }
}