﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace ABXY.EditorUIFramework
{
    public class ColoredSquare
    {
        public static void Draw(Rect position, Color color)
        {
            Texture2D texture = ColorTools.MakeTex(color);
            GUIStyle boxStyle = new GUIStyle(GUI.skin.box);
            boxStyle.normal.background = texture;
            GUI.Box(position, "", boxStyle);
        }

        
    }
}